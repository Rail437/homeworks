package homework17;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        String text = "Hello Hello my my my my friend  mymy        ";
        calculateWords(text);
    }

    private static void calculateWords(String text) {

        String[] words = text.split("\\s+");
        Map<String, Integer> result = new HashMap<>();
        for (String buff : words) {
            if (result.containsKey(buff)) {
                result.put(buff, result.get(buff) + 1);
            } else {
                result.put(buff, 1);
            }
        }
        printMap(result);
    }

    private static void printMap(Map<String, Integer> result) {
        for (Map.Entry<String, Integer> entry : result.entrySet()) {
            System.out.println( "Слово: "+ entry.getKey() + " встречается " + entry.getValue() + " раз.");
        }
    }
}
