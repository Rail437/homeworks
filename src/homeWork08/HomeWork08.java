package homeWork08;

import java.util.InputMismatchException;
import java.util.Scanner;

public class HomeWork08 {
    public static void main(String[] args) {
        int count = 10; //
        int i = 0;

        Human[] humans = new Human[count];
        while (i < count){
            try {
                Scanner scanner = new Scanner(System.in);
                Human human = new Human();
                System.out.println("Введите имя: ");
                String name = scanner.nextLine();
                human.setName(name);
                System.out.println("Введите вес: ");
                int weight = scanner.nextInt();
                human.setWeight(weight);
                humans[i] = human;
            }catch (InputMismatchException ex){
                ex.printStackTrace();
                i--;
            }
            i++;
        }
        sortByWeight(humans);
        i = 0;
        while (i < count){
            System.out.println(humans[i].toString());
            i++;
        }
    }

    private static void sortByWeight(Human[] humans){
        if(humans.length > 1) {
            int buff;
            Human temp;
            for(int a = 0; a < humans.length - 1; a++) {
                buff = a;
                for(int b = a + 1; b < humans.length; b++)
                    if(humans[b].getWeight() < humans[buff].getWeight())
                        buff = b;
                temp = humans[a];
                humans[a] = humans[buff];
                humans[buff] = temp;
            }
        }
    }

}
