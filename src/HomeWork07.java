import java.util.HashMap;
import java.util.Map;

public class HomeWork07 {
    public static void main(String[] args) {
        Integer[] num = {0, 1, 2, 3, 4, 5, 6,  1, 2, 3, 4, 5, 6, 1,7, 2, 3, 1, 2, 3, 7, -10, 1, 2, 3, 0, 1, 2, 0, 1, 2, 0, 1, 2, 2, 3, 4, 5, 6, -12, 3, 4, 5, 6, 12, 3, 4, 5, 6, -10, -1};
        System.out.println(finder(num));
    }

    private static Integer finder(Integer[] numbers) {
        Integer min = 0;
        Integer minValue;
        Map<Integer, Integer> integerMap = new HashMap<>();
        for (int i = 0; numbers[i] != -1 ; i++) {
            if (integerMap.get(numbers[i]) != null) {
                integerMap.put(numbers[i], integerMap.get(numbers[i]) + 1);
            } else {
                integerMap.putIfAbsent(numbers[i], 1);
            }
        }
        minValue = integerMap.get(numbers[0]);
        for (Map.Entry<Integer, Integer> entry : integerMap.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(minValue > value) {
                minValue = value;
                min = key;
            }
        }
        System.out.println(integerMap);

        return min;
    }
}
