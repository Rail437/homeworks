package HomeWork11;

public class Logger {
    private static Logger instance;
    private String message;

    private Logger(String value) {
        this.message = value;
    }

    public static Logger getInstance(String message) {
        if (instance == null) {
            instance = new Logger(message);
        }
        return instance;
    }

    public void log(String text){
        System.out.println(message);
    }
}
