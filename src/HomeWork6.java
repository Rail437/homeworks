public class HomeWork6 {
    public static void main(String[] args) {
        int[] numbers = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        System.out.println(findNumber(numbers, 20));
        sortNumbers(numbers);
    }

    private static int findNumber(int[] numbers, int num) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == num) {
                return i;
            }
        }
        return -1;
    }

    private static void sortNumbers(int[] numbers) {
        for (int j = 0; j < numbers.length; j++) {
            for (int i = 0; i < numbers.length - 1; i++) {
                if (numbers[i] == 0 && numbers[i + 1] != 0) {
                    int buff = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = buff;
                }
            }
        }
    }
}
