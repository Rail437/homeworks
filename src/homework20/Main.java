package homework20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String color = "black";
        String model = "ford";
        int mileage = 133;
        printList(findCarByColorAndMileage(color, mileage));
        System.out.println("===============================================");
        System.out.println(findUniqueCarByCoast(70000, 99000));
        System.out.println("===============================================");
        System.out.println(findColorCarByMinPrice());
        System.out.println("===============================================");
        System.out.println(findAverageCost(model));
    }

    private static void printList(List<String> strings) {
        for (String str : strings) {
            System.out.println(str);
        }
    }


    /**
     * Задача 1.Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
     */
    private static List<String> findCarByColorAndMileage(String color, int milage) {
        List<String> stringList = new ArrayList<>();
        List<String[]> buffer = getStrings()
                .stream()
                .filter(strings -> strings[2].equalsIgnoreCase(color) ||
                        strings[3].equalsIgnoreCase(String.valueOf(milage)))
                .peek(strings -> stringList.add(strings[0])).collect(Collectors.toList());
        return stringList;
    }


    /**
     * Задача 2.Количество уникальных моделей в ценовом диапазоне от [number] до [number] тыс.
     */
    private static int findUniqueCarByCoast(int min, int max) {
        List<String> stringList = new ArrayList<>();
        List<String[]> buffer = getStrings()
                .stream()
                .filter(strings -> Integer.parseInt(strings[4]) >= min &
                        Integer.parseInt(strings[4]) <= max)
                .peek(strings -> stringList.add(strings[1])).collect(Collectors.toList());
        return (int) stringList.stream().distinct().count();
    }

    /**
     * Задача 3.Вывести цвет автомобиля с минимальной стоимостью.
     */
    private static String findColorCarByMinPrice() {
        List<String[]> buffer = getStrings();
        String color = buffer.get(0)[2];
        int min = Integer.parseInt(buffer.get(0)[4]);
        for (String[] find : buffer) {
            if (Integer.parseInt(find[4]) < min) {
                color = find[2];
            }
        }
        return color;
    }

    /**
     * Задача 4. Среднюю стоимость [Model]
     */
    private static int findAverageCost(String model) {
        int result = 0;
        List<String[]> buffer = getStrings().stream()
                .filter(strings -> model.equalsIgnoreCase(strings[1]))
                .collect(Collectors.toList());
        for (String[] find : buffer) {
            result += Integer.parseInt(find[4]);
        }
        return result / buffer.size();
    }


    private static List<String[]> getStrings() {
        List<String[]> buffer = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("src/Cars.txt"))) {
            reader.lines()
                    .forEach(line -> buffer.add(line.split("\\|")));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return buffer;
    }

}
