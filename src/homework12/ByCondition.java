package homework12;

@FunctionalInterface
public interface ByCondition {
    boolean isOk(int number);
}