package homework12;


import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] box = {123,234,345,456,777,888};

        ByCondition CheckDigit = number -> (number % 2) == 0;

        ByCondition CheckNumber = number -> {
            int buf;
            int result = 0;
            while (number > 10){
                buf = number % 10;
                result = result + buf;
                number = number / 10;
            }
            result = result + number;
            return result % 2 == 0;
        };

        System.out.println(Arrays.toString(Sequence.filter(box, CheckNumber)));
        System.out.println(Arrays.toString(Sequence.filter(box, CheckDigit)));
    }
}

