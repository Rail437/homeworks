package homework12;

import java.util.ArrayList;
import java.util.List;

public class Sequence{
    public static int[] filter(int[] array, ByCondition condition) {
        //Можно было и не делать так, а сразу использовать массив с длиной array.length,
        //но тогда в возвращаемом массиве вконце будут пустые ячейки.
        //Что лучше, массив с пустыми ячейками или использование дополнительных ресурсов при работе?
        List<Integer> list = new ArrayList<>();
        for (int j : array) {
            if (condition.isOk(j)) {
                list.add(j);
            }
        }
        int[] result = new int[list.size()];
        for(int i = 0; i < list.size(); i++){
            result[i] = list.get(i);
        }
        return result;
    }
}
