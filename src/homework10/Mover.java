package homework10;

@FunctionalInterface
public interface Mover {
    void move(int x, int y);
}
