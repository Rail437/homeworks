package homework10;

import homework09.Circle;
import homework09.Figure;
import homework09.Square;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Mover[] figures = {new Square(10, 23, 23),
                new Circle(20, 15, 20),
                new Square(20, 13, 3),
                new Circle(0, 10, 2)};

        Arrays.stream(figures).forEach(o -> o.move(0,0) );
    }
}
