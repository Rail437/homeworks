package homework19;


import java.util.List;

public class Main {
    public static void main(String[] args) {
        String txt = "src/users.txt";
        UsersRepository usersRepository = new UsersRepositoryFileImpl(txt);

        printList(usersRepository.findAll());
        System.out.println();
        printList(usersRepository.findByAge(22));
        System.out.println();
        printList(usersRepository.findByIsWorkerIsTrue());
    }

    private static void printList(List<User> users) {
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }
}

