package homework19;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        return getUsers();
    }

    @Override
    public void save(User user) {
        try (Writer writer = new FileWriter(fileName, true); BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> users = getUsers();
        List<User> result = new ArrayList<>();
        for (User user : users){
            if(user.getAge() == age){
                result.add(user);
            }
        }
        return result;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> users = getUsers();
        List<User> result = new ArrayList<>();
        for (User user : users){
            if(user.isWorker()){
                result.add(user);
            }
        }
        return result;
    }

    private List<User> getUsers() {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {
            // создали читалку на основе файла
            // создали буферизированную читалку
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                // берем имя
                String name = parts[0];
                // берем возраст
                int age = Integer.parseInt(parts[1]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                // создаем нового человека
                User newUser = new User(name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        // этот блок выполнится точно
        // пытаемся закрыть ресурсы
        // пытаемся закрыть ресурсы
        return users;
    }



}

