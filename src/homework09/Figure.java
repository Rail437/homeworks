package homework09;

public abstract class Figure {
    private int x;
    private int y;


    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getPerimeter(){
        return 0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
