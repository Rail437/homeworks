package homework09;

public class Rectangle extends Figure{
    private double sideA;
    private double sideB;

    public Rectangle(int x, int y, double sideA, double sideB) {
        super(x, y);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public double getPerimeter() {
        return (sideA * 2) + (sideB * 2);
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }
}
