package homework09;

import homework10.Mover;

public class Square extends Rectangle implements Mover {

    public Square(int x, int y, double side) {
        super(x, y, side, side);
    }


    @Override
    public void move(int x, int y) {
        System.out.println("Обьект " + this.getClass().getSimpleName() + " перемещен к координатам Х: " + x + " Y: " + y);
        this.setX(x);
        this.setY(y);
    }
}
