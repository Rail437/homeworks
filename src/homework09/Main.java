package homework09;

public class Main {
    public static void main(String[] args) {

        Figure ellipse = new Ellipse(0,0,12,15);
        Figure circle = new Circle(0,0,10);
        Figure rectangle = new Rectangle(0,0,10, 15);
        Figure scuare = new Square (0,0,10);

        System.out.println("ellipse perimeter: " + ellipse.getPerimeter());
        System.out.println("circle perimeter: " + circle.getPerimeter());
        System.out.println("rectangle perimeter: " + rectangle.getPerimeter());
        System.out.println("scuare perimeter: " + scuare.getPerimeter());
    }
}
