package homework09;

import homework10.Mover;

public class Circle extends Ellipse implements Mover {
    public Circle(int x, int y, double radius1) {
        super(x, y, radius1, radius1);
    }

    @Override
    public void move(int x, int y) {
        System.out.println("Обьект " + this.getClass().getSimpleName() + " перемещен к координатам Х: " + x + " Y: " + y);
        this.setX(x);
        this.setY(y);
    }
}
