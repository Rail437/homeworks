package homework16;

public class Main {
    public static void main(String[] args) {
        ArrayListTest();
        LinkedListTest();
    }

    private static void LinkedListTest() {
        LinkedList<Integer> numbers = new LinkedList<>();
        numbers.add(45);
        numbers.add(78);
        numbers.add(10);
        numbers.add(17);
        numbers.add(89);
        numbers.add(16);
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(1));
        System.out.println(numbers.get(2));
        System.out.println(numbers.get(3));
        System.out.println(numbers.get(-1));
    }

    private static void ArrayListTest() {
        ArrayList<Integer> numbers = new ArrayList<>();
        //45, 78, 10, 17, 89, 16
        numbers.add(45);
        numbers.add(78);
        numbers.add(10);
        numbers.add(17);
        numbers.add(89);
        numbers.add(16);
        printList(numbers);
        numbers.removeAt(3);
        printList(numbers);
    }

    private static void printList(ArrayList<Integer> numbers) {
        for (int i = 0; i < numbers.length(); i++) {
            System.out.print(numbers.get(i) + ", ");
        }
        System.out.println();
    }
}
