public class HomeWork5 {
    public static void main(String[] args) {
        int[] numbers = {356,457,228,452,321,-1};
        System.out.println(findMinNumber(numbers));
    }

    private static int findMinNumber(int[] num){
        int min = checkNumber(num[0]);
        int i = 1;
        while (num[i] != -1){
            int buf = checkNumber(num[i]);
            if(buf <= min) {
                min = buf;
            }
            i++;
        }
        return min;
    }

    private static int checkNumber(int number){
        int min = number % 10;
        while (number >= 10){
            int buff = number % 10;
            if(buff <= min){
                min = buff;
            }
            number = number / 10;
        }
        if(number < min){
            min = number;
        }
        return min;
    }
}
